help:
	@go run ./cmd/main.go help
tidy:
	@go mod tidy
download:
	@go mod download
test:
	@go test -v ./...
run: tidy
	@go run ./cmd/main.go api
migration:
	@go run ./cmd/migrator/main.go up
migration-down:
	@go run ./cmd/migrator/main.go down
scan:
	@script/gosec.sh
build:
	@go build \
		-ldflags "-X main.buildName=health-service -X main.buildVersion=`git rev-parse --short HEAD`" \
		-o technical-test-skyshi.app cmd/main.go
docker-build:
	@docker build -f Dockerfile -t "faltar/devcode-test-skyshi:16.0" --build-arg BUILD_DATE="$(date -u +"%Y-%m-%dT%H:%M:%SZ")" .
docker-run-container:
	@docker container run -ti --init --rm \
     		--name technical-test-skyshi \
     		-e MYSQL_HOST=172.17.0.1 \
     		-e MYSQL_PORT=3306 \
     		-e MYSQL_USER=root \
     		-e MYSQL_PASSWORD=root \
     		-e MYSQL_DBNAME=todo4 \
     		--memory="1g" --cpus="1" \
     		--net host \
     		faltar/devcode-test-skyshi:16.0 ps
docker-push:
	@docker push faltar/devcode-test-skyshi:16.0
docker-run:
	@docker run --name technical-test-skyshi -e MYSQL_HOST=172.17.0.1 -e MYSQL_PORT=3334 -e MYSQL_USER=root -e MYSQL_PASSWORD=root -e MYSQL_DBNAME=test-db --memory="1g" --cpus="1" -p 3030:3030 technical-test-skyshi:1.0
docker-container-remove:
	@docker rm -f technical-test-skyshi
docker-image-remove:
	@docker image rm technical-test-skyshi:1.0