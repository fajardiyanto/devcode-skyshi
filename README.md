# Technical Test Privy
#### This project auto generate using [rest-api-generator](https://github.com/fajarardiyanto/rest-api-generator)

## Getting started
Required Golang 1.17+
### Run
```bash
make run
```

### Build Docker
```bash
make docker-build
```

### Run Docker
```bash
make build-run
```

#### Tips
Maybe it would be better to do some basic code scanning before pushing to the repository.
```sh
# for *.nix users just run gosec.sh
# curl is required
# more information https://github.com/securego/gosec
make scan
```

### Postman
[GetPostman](https://www.getpostman.com/collections/a3bd9324d8e237f5b580)

```shell
Set Authorization Bearer Token in Workspace Collection
```

#### SignUp
```shell
curl --location --request POST '127.0.0.1:8081/sign-up' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NjgwMDAzNTUsInVzZXJfaWQiOjN9.r13ciRtVWVL2TGaSCTChhAs03G3fRuzG15BvZ6QrQ-Y' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "fajar",
    "email": "fajar@gmail.com",
    "password": "fajar"
}'
```

#### Login
```shell
curl --location --request POST '127.0.0.1:8081/login' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NjgwMDAzNTUsInVzZXJfaWQiOjN9.r13ciRtVWVL2TGaSCTChhAs03G3fRuzG15BvZ6QrQ-Y' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "ardi",
    "password": "ardi"
}'
```

#### Logout
```shell
curl --location --request POST '127.0.0.1:8081/logout' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NjgwMDAzNTUsInVzZXJfaWQiOjN9.r13ciRtVWVL2TGaSCTChhAs03G3fRuzG15BvZ6QrQ-Y'
```

#### Balance TopUp
```shell
curl --location --request POST '127.0.0.1:8081/topup/balance' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NjgwMDAzNTUsInVzZXJfaWQiOjN9.r13ciRtVWVL2TGaSCTChhAs03G3fRuzG15BvZ6QrQ-Y' \
--header 'Content-Type: application/json' \
--data-raw '{
    "balance": 10000
}'
```

#### Balance Transfer
```shell
curl --location --request POST '127.0.0.1:8081/transfer/balance' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NjgwMDAzNTUsInVzZXJfaWQiOjN9.r13ciRtVWVL2TGaSCTChhAs03G3fRuzG15BvZ6QrQ-Y' \
--header 'Content-Type: application/json' \
--data-raw '{
    "balance": 2000,
    "account_receive": 1
}'
```