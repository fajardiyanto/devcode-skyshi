package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"technical-test-skyshi/config"
	"technical-test-skyshi/internal/controller"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/services"
	"technical-test-skyshi/pkg/middleware"
	"technical-test-skyshi/pkg/util"
)

func main() {
	// init configuration database
	config.Init()

	r := mux.NewRouter()

	r.Use(middleware.MiddlewareError)

	// Service
	svcHealth := services.NewHealthInfoService()
	svcActivity := services.NewActivityService()
	svcTodo := services.NewTodoService()

	// Controller
	ctrlHealth := controller.NewHealthInfoController(svcHealth)
	ctrlActivity := controller.NewActivityController(svcActivity)
	ctrlTodo := controller.NewTodoController(svcTodo)

	r.HandleFunc("", ctrlHealth.HealthInfo).Methods("GET")

	r.HandleFunc("/activity-groups", ctrlActivity.GetActivity).Methods("GET")
	r.HandleFunc("/activity-groups/{id}", ctrlActivity.GetActivityByID).Methods("GET")
	r.HandleFunc("/activity-groups", ctrlActivity.CreateActivity).Methods("POST")
	r.HandleFunc("/activity-groups/{id}", ctrlActivity.UpdateActivity).Methods("PATCH")
	r.HandleFunc("/activity-groups/{id}", ctrlActivity.DeleteActivity).Methods("DELETE")

	r.HandleFunc("/todo-items", ctrlTodo.GetTodo).Methods("GET")
	r.HandleFunc("/todo-items/{id}", ctrlTodo.GetTodoByID).Methods("GET")
	r.HandleFunc("/todo-items", ctrlTodo.CreateTodo).Methods("POST")
	r.HandleFunc("/todo-items/{id}", ctrlTodo.UpdateTodo).Methods("PATCH")
	r.HandleFunc("/todo-items/{id}", ctrlTodo.DeleteTodo).Methods("DELETE")

	s := http.Server{
		Addr:    fmt.Sprintf(":%s", entity.GetConfig().Host),
		Handler: r,
	}

	util.GracefullyShutdown(&s)
}
