package main

import (
	"fmt"
	"log"
	"os"
	"technical-test-skyshi/cmd/migrator/internal"
	"technical-test-skyshi/config"
	"technical-test-skyshi/internal/entity"
)

func main() {
	config.Init()

	sqlDB, err := config.GetDB().Orm().DB()
	if err != nil {
		config.GetLogger().Error(err).Quit()
	}

	config.GetLogger().Info(entity.GetConfig().MigrationDir)

	mg := internal.NewMigrator(sqlDB, entity.GetConfig().MigrationDir, os.Stdout)

	args := os.Args[1:]

	switch args[0] {
	case "create":
		if len(args) < 2 {
			log.Fatalln("migration name is required")
		}
		err = mg.Create(args[1])
	case "status":
		err = mg.Status()
	case "up":
		err = mg.Up()
	case "down":
		err = mg.Down()
	case "reset":
		err = mg.Reset()
	case "help":
		fallthrough
	default:
		fmt.Print(usage)
	}

	if err != nil {
		log.Fatalln(err)
	}

	config.GetLogger().Success("Success Migration")
}

const usage = `
Migration Tool Help

migrate <command> [<args>]

Command:
  help                  show this help.
  up                    apply migration.
  down                  undo migration.
  reset                 reset migration history
  status                show migration status.
  create <name>         create a new migration file.
`
