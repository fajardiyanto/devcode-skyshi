package config

import (
	databaseinterface "github.com/fajarardiyanto/flt-go-database/interfaces"
	databaseLib "github.com/fajarardiyanto/flt-go-database/lib"
	"technical-test-skyshi/internal/entity"
)

var database databaseinterface.SQL

type ResultTable struct {
	Field   string
	Type    string
	Null    string
	Key     string
	Default string
	Extra   string
}

func InitMysql(db databaseinterface.Database) {
	database = db.LoadSQLDatabase(entity.GetConfig().Database.Mysql)

	if err := database.LoadSQL(); err != nil {
		logger.Error(err).Quit()
	}

	if err := database.Orm().AutoMigrate(
		&entity.ActivityMigrationModel{},
		&entity.TodoMigrationModel{},
	); err != nil {
		logger.Error(err).Quit()
	} else {
		logger.Success("success migration table")
	}
}

func Init() {
	db := databaseLib.NewLib()
	db.Init(GetLogger())

	InitMysql(db)
}

func GetDB() databaseinterface.SQL {
	return database
}
