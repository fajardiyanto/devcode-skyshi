package config

import (
	"github.com/fajarardiyanto/flt-go-logger/interfaces"
	"github.com/fajarardiyanto/flt-go-logger/lib"
	"technical-test-skyshi/internal/entity"
)

var logger interfaces.Logger

func GetLogger() interfaces.Logger {
	return logger
}

func init() {
	logger = lib.NewLib()
	logger.Init(entity.GetConfig().Name)
}
