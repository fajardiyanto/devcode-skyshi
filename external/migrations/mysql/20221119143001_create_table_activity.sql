-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS activites (
    id bigint auto_increment primary key ,
    email varchar(255) not null unique ,
    title varchar(255) not null ,
    created_at timestamp ,
    updated_at timestamp ,
    deleted_at timestamp
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS activites;
-- +goose StatementEnd
