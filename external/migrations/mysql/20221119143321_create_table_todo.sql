-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS todos (
    id bigint auto_increment primary key ,
    activity_group_id varchar(255) not null ,
    title varchar(255) not null ,
    is_active varchar(255) not null ,
    priority varchar(255) not null ,
    created_at timestamp ,
    updated_at timestamp ,
    deleted_at timestamp
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS todos;
-- +goose StatementEnd
