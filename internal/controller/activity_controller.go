package controller

import (
	"fmt"
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	parser "github.com/fajarardiyanto/flt-go-utils/parser"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/repo"
	"technical-test-skyshi/pkg/util/validate"
)

type ActivityController struct {
	svc repo.ActivityRepositoryService
}

func NewActivityController(svc repo.ActivityRepositoryService) *ActivityController {
	return &ActivityController{
		svc: svc,
	}
}

func (c *ActivityController) GetActivity(w http.ResponseWriter, r *http.Request) {
	res, err := c.svc.GetActivity()
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "failed",
			Message: err.Error(),
			Data:    res,
		})
		return
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *ActivityController) GetActivityByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	res, err := c.svc.GetOne(id)
	if err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Activity with ID %s Not Found", id),
				Data:    map[string]interface{}{},
			})
			return
		}
	}
	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *ActivityController) CreateActivity(w http.ResponseWriter, r *http.Request) {
	var data entity.ActivityModel
	if err := interfaces.GetJSON(r, &data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	if err := validate.Struct(data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	res, err := c.svc.CreateActivity(data)
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	interfaces.JSON(w, http.StatusCreated, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *ActivityController) UpdateActivity(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	getAct, err := c.svc.GetOne(id)
	if err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Activity with ID %s Not Found", id),
				Data:    map[string]interface{}{},
			})
			return
		}
	}

	var data entity.ActivityModel
	if err := interfaces.GetJSON(r, &data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	if err := validate.Struct(data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	data.Id = parser.StrToInt(id)
	res, err := c.svc.UpdateActivity(data)
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	res.CreatedAt = getAct.CreatedAt
	res.Email = getAct.Email

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *ActivityController) DeleteActivity(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	if _, err := c.svc.GetOne(id); err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Activity with ID %s Not Found", id),
				Data:    map[string]interface{}{},
			})
			return
		}
	}

	if err := c.svc.Delete(id); err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    map[string]interface{}{},
	})
	return
}
