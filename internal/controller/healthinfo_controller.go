package controller

import (
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"net/http"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/repo"
)

type HealthInfoController struct {
	svc repo.HealthInfoRepositoryService
}

func NewHealthInfoController(svc repo.HealthInfoRepositoryService) *HealthInfoController {
	return &HealthInfoController{
		svc: svc,
	}
}

func (c *HealthInfoController) HealthInfo(w http.ResponseWriter, r *http.Request) {
	res := &entity.ResponseHealthInfo{
		ServiceName:    entity.GetConfig().Name,
		ServiceVersion: entity.GetConfig().Version,
		Compiler:       c.svc.BuildInfo(),
		Database: entity.Database{
			Mysql: c.svc.HealthInfoDatabase(),
		},
	}

	interfaces.JSON(w, http.StatusOK, res)
	return
}
