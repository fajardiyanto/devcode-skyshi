package controller

import (
	"fmt"
	"github.com/fajarardiyanto/flt-go-router/interfaces"
	"github.com/fajarardiyanto/flt-go-utils/parser"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/repo"
	"technical-test-skyshi/pkg/util/validate"
)

type TodoController struct {
	svc repo.TodoRepositoryService
}

func NewTodoController(svc repo.TodoRepositoryService) *TodoController {
	return &TodoController{
		svc: svc,
	}
}

func (c *TodoController) GetTodo(w http.ResponseWriter, r *http.Request) {
	id := interfaces.GetQuery(r, "activity_group_id")

	res, err := c.svc.GetTodo(id)
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *TodoController) GetTodoByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	res, err := c.svc.GetOne(id)
	if err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Todo with ID %s Not Found", id),
				Data:    map[string]interface{}{},
			})
			return
		}
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *TodoController) CreateTodo(w http.ResponseWriter, r *http.Request) {
	var data entity.TodoModel
	if err := interfaces.GetJSON(r, &data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	if err := validate.Struct(data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	if data.Priority == "" {
		data.Priority = "very-high"
	}

	res, err := c.svc.CreateTodo(data)
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	interfaces.JSON(w, http.StatusCreated, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *TodoController) UpdateTodo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	getAct, err := c.svc.GetOne(id)
	if err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Todo with ID %s Not Found", id),
				Data:    map[string]interface{}{},
			})
			return
		}
	}

	var data entity.TodoRequestModel
	if err = interfaces.GetJSON(r, &data); err != nil {
		interfaces.JSON(w, http.StatusBadRequest, entity.ResponseModel{
			Status:  "Bad Request",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	dt := entity.TodoModel{
		Id:              parser.StrToInt(id),
		Title:           data.Title,
		ActivityGroupId: data.ActivityGroupId,
		IsActive:        data.IsActive,
	}

	res, err := c.svc.UpdateTodo(dt)
	if err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	res.CreatedAt = getAct.CreatedAt

	if data.ActivityGroupId == 0 {
		res.ActivityGroupId = getAct.ActivityGroupId
	}

	if res.Priority == "" {
		res.Priority = getAct.Priority
	}

	if data.Title == "" {
		res.Title = getAct.Title
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    res,
	})
	return
}

func (c *TodoController) DeleteTodo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	getAct, err := c.svc.GetOne(id)
	if err != nil {
		if strings.Contains("record not found", err.Error()) {
			interfaces.JSON(w, http.StatusNotFound, entity.ResponseModel{
				Status:  "Not Found",
				Message: fmt.Sprintf("Todo with ID %s Not Found", id),
				Data:    getAct,
			})
			return
		}
	}

	if err = c.svc.Delete(id); err != nil {
		interfaces.JSON(w, http.StatusInternalServerError, entity.ResponseModel{
			Status:  "Failed",
			Message: err.Error(),
			Data:    map[string]interface{}{},
		})
		return
	}

	interfaces.JSON(w, http.StatusOK, entity.ResponseModel{
		Status:  "Success",
		Message: "Success",
		Data:    map[string]interface{}{},
	})
	return
}
