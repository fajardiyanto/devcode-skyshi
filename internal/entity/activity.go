package entity

import "time"

type ActivityModel struct {
	Id        int     `json:"id" gorm:"column:id"`
	Email     string  `json:"email" gorm:"column:email"`
	Title     string  `json:"title" gorm:"column:title" validate:"required"`
	CreatedAt string  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt string  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt *string `json:"deleted_at" gorm:"column:deleted_at"`
}

type ActivityMigrationModel struct {
	Id        int       `json:"id" gorm:"index;column:id"`
	Email     string    `json:"email" gorm:"column:email"`
	Title     string    `json:"title" gorm:"column:title"`
	CreatedAt time.Time `json:"created_at" gorm:"column:created_at"`
	UpdatedAt time.Time `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

func (*ActivityMigrationModel) TableName() string {
	return "activities"
}

func (*ActivityModel) TableName() string {
	return "activities"
}
