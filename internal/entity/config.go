package entity

import (
	"github.com/fajarardiyanto/flt-go-database/interfaces"
	"github.com/fajarardiyanto/flt-go-utils/flags"
	"os"
	"strconv"
)

var cfg = new(Config)

type Config struct {
	Version      string `yaml:"version"`
	Name         string `yaml:"name"`
	Host         string `yaml:"host"`
	MigrationDir string `yaml:"migrationDir" default:"external"`
	Database     struct {
		Mysql interfaces.SQLConfig `yaml:"mysql"`
	} `yaml:"database"`
}

func init() {
	flags.Init("config/config.yaml", cfg)

	if os.Getenv("MYSQL_HOST") != "" {
		cfg.Database.Mysql.Host = os.Getenv("MYSQL_HOST")
	}

	if os.Getenv("MYSQL_PORT") != "" {
		port, _ := strconv.Atoi(os.Getenv("MYSQL_PORT"))
		cfg.Database.Mysql.Port = port
	}

	if os.Getenv("MYSQL_USER") != "" {
		cfg.Database.Mysql.Username = os.Getenv("MYSQL_USER")
	}

	if os.Getenv("MYSQL_PASSWORD") != "" {
		cfg.Database.Mysql.Password = os.Getenv("MYSQL_PASSWORD")
	}

	if os.Getenv("MYSQL_DBNAME") != "" {
		cfg.Database.Mysql.Database = os.Getenv("MYSQL_DBNAME")
	}
}

func GetConfig() *Config {
	return cfg
}
