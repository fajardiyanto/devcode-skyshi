package entity

import "time"

type TodoModel struct {
	Id              int     `json:"id" gorm:"column:id" gorm:"column:id"`
	ActivityGroupId int     `json:"activity_group_id" gorm:"column:activity_group_id" validate:"required"`
	Title           string  `json:"title" gorm:"column:title" validate:"required"`
	IsActive        bool    `json:"is_active" gorm:"column:is_active;default:true"`
	Priority        string  `json:"priority" gorm:"column:priority"`
	CreatedAt       string  `json:"created_at" gorm:"column:created_at"`
	UpdatedAt       string  `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt       *string `json:"deleted_at" gorm:"column:deleted_at"`
}

type TodoMigrationModel struct {
	Id              int       `json:"id" gorm:"index;column:id"`
	ActivityGroupId int       `json:"activity_group_id" gorm:"index;column:activity_group_id"`
	Title           string    `json:"title" gorm:"column:title"`
	IsActive        bool      `json:"is_active" gorm:"column:is_active;default:true"`
	Priority        string    `json:"priority" gorm:"column:priority"`
	CreatedAt       time.Time `json:"created_at" gorm:"column:created_at"`
	UpdatedAt       time.Time `json:"updated_at" gorm:"column:updated_at"`
	DeletedAt       time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

type TodoRequestModel struct {
	ActivityGroupId int    `json:"activity_group_id"`
	Title           string `json:"title"  validate:"required"`
	IsActive        bool   `json:"is_active"`
}

func (*TodoMigrationModel) TableName() string {
	return "todos"
}

func (*TodoModel) TableName() string {
	return "todos"
}
