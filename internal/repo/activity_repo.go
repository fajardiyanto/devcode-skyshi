package repo

import "technical-test-skyshi/internal/entity"

type ActivityRepositoryService interface {
	GetActivity() (*[]entity.ActivityModel, error)
	GetOne(string) (*entity.ActivityModel, error)
	CreateActivity(entity.ActivityModel) (*entity.ActivityModel, error)
	UpdateActivity(entity.ActivityModel) (*entity.ActivityModel, error)
	Delete(id string) error
}
