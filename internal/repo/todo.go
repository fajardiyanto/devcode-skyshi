package repo

import "technical-test-skyshi/internal/entity"

type TodoRepositoryService interface {
	GetTodo(q string) (*[]entity.TodoModel, error)
	GetOne(string) (*entity.TodoModel, error)
	CreateTodo(entity.TodoModel) (*entity.TodoModel, error)
	UpdateTodo(entity.TodoModel) (*entity.TodoModel, error)
	Delete(id string) error
}
