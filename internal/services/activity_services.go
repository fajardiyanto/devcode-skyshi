package services

import (
	"technical-test-skyshi/config"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/repo"
	"time"
)

type activityService struct{}

func NewActivityService() repo.ActivityRepositoryService {
	return &activityService{}
}

func (a *activityService) GetActivity() (*[]entity.ActivityModel, error) {
	var res []entity.ActivityModel
	if err := config.GetDB().Orm().Debug().Model(&entity.ActivityModel{}).Find(&res).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &res, nil
}

func (a *activityService) GetOne(id string) (*entity.ActivityModel, error) {
	var res *entity.ActivityModel
	if err := config.GetDB().Orm().Debug().Model(&entity.ActivityModel{}).Where("id = ?", id).First(&res).Error; err != nil {
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (a *activityService) CreateActivity(req entity.ActivityModel) (*entity.ActivityModel, error) {
	now := time.Now().Format("2006-01-02 15:04:05")
	req.CreatedAt = now
	req.UpdatedAt = now

	if err := config.GetDB().Orm().Debug().Model(&entity.ActivityModel{}).Create(&req).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &req, nil
}

func (a *activityService) UpdateActivity(req entity.ActivityModel) (*entity.ActivityModel, error) {
	req.UpdatedAt = time.Now().Format("2006-01-02 15:04:05")

	if err := config.GetDB().Orm().Debug().Model(&entity.ActivityModel{}).Where("id = ?", req.Id).Updates(&req).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &req, nil
}

func (a *activityService) Delete(id string) error {
	if err := config.GetDB().Orm().Debug().Where("id = ?", id).Delete(&entity.ActivityModel{}).Error; err != nil {
		config.GetLogger().Error(err)
		return err
	}

	return nil
}
