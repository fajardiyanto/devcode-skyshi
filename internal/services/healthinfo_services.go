package services

import (
	"fmt"

	"technical-test-skyshi/config"

	"github.com/shirou/gopsutil/v3/host"
	"technical-test-skyshi/internal/repo"
)

type service struct{}

func NewHealthInfoService() repo.HealthInfoRepositoryService {
	return &service{}
}

func (*service) BuildInfo() (OSBuildName string) {
	if info, err := host.Info(); err == nil {
		OSBuildName = fmt.Sprintf("%s (%s)",
			info.PlatformFamily, info.PlatformVersion)
	}

	return OSBuildName
}

func (*service) HealthInfoDatabase() bool {
	var version string
	err := config.GetDB().Orm().Raw("SELECT @@VERSION").Scan(&version).Error
	if err != nil {
		return false
	}

	return true
}
