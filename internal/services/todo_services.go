package services

import (
	"technical-test-skyshi/config"
	"technical-test-skyshi/internal/entity"
	"technical-test-skyshi/internal/repo"
	"time"
)

type todoService struct{}

func NewTodoService() repo.TodoRepositoryService {
	return &todoService{}
}

func (a *todoService) GetTodo(q string) (*[]entity.TodoModel, error) {
	var res []entity.TodoModel

	db := config.GetDB().Orm().Debug()
	if q != "" {
		db.Where("activity_group_id = ?", q)
	}

	if err := db.Model(&entity.TodoModel{}).Find(&res).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &res, nil
}

func (a *todoService) GetOne(id string) (*entity.TodoModel, error) {
	var res *entity.TodoModel
	if err := config.GetDB().Orm().Debug().Model(&entity.TodoModel{}).Where("id = ?", id).First(&res).Error; err != nil {
		config.GetLogger().Error(err)
		return res, err
	}

	return res, nil
}

func (a *todoService) CreateTodo(req entity.TodoModel) (*entity.TodoModel, error) {
	now := time.Now().Format("2006-01-02 15:04:05")
	req.CreatedAt = now
	req.UpdatedAt = now

	if err := config.GetDB().Orm().Debug().Model(&entity.TodoModel{}).Create(&req).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &req, nil
}

func (a *todoService) UpdateTodo(req entity.TodoModel) (*entity.TodoModel, error) {
	req.UpdatedAt = time.Now().Format("2006-01-02 15:04:05")

	if err := config.GetDB().Orm().Debug().Model(&entity.TodoModel{}).Where("id = ?", req.Id).Updates(&req).Error; err != nil {
		config.GetLogger().Error(err)
		return nil, err
	}

	return &req, nil
}

func (a *todoService) Delete(id string) error {
	if err := config.GetDB().Orm().Debug().Where("id = ?", id).Delete(&entity.TodoModel{}).Error; err != nil {
		config.GetLogger().Error(err)
		return err
	}

	return nil
}
