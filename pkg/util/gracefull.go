package util

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"technical-test-skyshi/config"
)

func GracefullyShutdown(s *http.Server) {
	idleConnectionsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint
		if err := s.Shutdown(context.Background()); err != nil {
			config.GetLogger().Error("HTTP Server Shutdown Error: %v", err).Quit()
		}
		close(idleConnectionsClosed)
	}()

	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		config.GetLogger().Error("HTTP server ListenAndServe Error: %v", err).Quit()
	}

	<-idleConnectionsClosed

	config.GetLogger().Success("HTTP server ListenAndServe success Shutting Down ...")
}
