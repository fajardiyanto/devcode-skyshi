package util

import "strconv"

func StrToInt(s string) int64 {
	i, _ := strconv.ParseInt(s, 10, 64)
	return i
}

func IntToStr(i int64) string {
	return strconv.Itoa(int(i))
}
