package validate

import (
	"errors"
	"reflect"
	"strings"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslation "github.com/go-playground/validator/v10/translations/en"
)

// validate holds the settings and caches for validating request struct values.
var validate *validator.Validate

// translator is a cache of locale and translation information.
var translator ut.Translator

func init() {

	// Instantiate a validator.
	validate = validator.New()

	// Create a translator for english so the error messages are
	// more human-readable than technical.
	translator, _ = ut.New(en.New(), en.New()).GetTranslator("en")

	// Register the english error messages for use.
	err := enTranslation.RegisterDefaultTranslations(validate, translator)
	if err != nil {
		panic(err)
	}

	err = validate.RegisterValidation("coexist", Coexist)
	if err != nil {
		panic(err)
	}

	// Use JSON tag names for errors instead of Go struct names.
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})
}

// Struct validates the provided model against it's declared tags.
func Struct(val interface{}) error {
	if err := validate.Struct(val); err != nil {

		// Use a type assertion to get the real error value.
		validationErrors, ok := err.(validator.ValidationErrors)
		if !ok {
			return err
		}

		for _, verror := range validationErrors {
			return errors.New(verror.Field() + " cannot be null")
		}

	}

	return nil
}

// Slice Struct validates the provided model against it's declared tags.
func Slice(val interface{}) error {
	if err := validate.Var(val, "dive"); err != nil {

		// Use a type assertion to get the real error value.
		validationErrors, ok := err.(validator.ValidationErrors)
		if !ok {
			return err
		}

		for _, verror := range validationErrors {
			return errors.New(verror.Translate(translator))
		}

	}

	return nil
}

func Coexist(fl validator.FieldLevel) bool {
	t := fl.Top()
	var tv reflect.Value
	if t.Kind() == reflect.Ptr {
		tv = t.Elem()
	} else {
		tv = t
	}

	f := tv.FieldByName(fl.Param())
	if !f.IsValid() {
		return false
	}

	var f2_is_zero bool
	switch f.Kind() {
	case reflect.Ptr, reflect.Struct, reflect.Slice, reflect.Map, reflect.Array:
		f2_is_zero = f.IsNil()
	default:
		f2_is_zero = f.IsZero()
	}

	var f1_is_zero bool
	switch fl.Field().Kind() {
	case reflect.Ptr, reflect.Struct, reflect.Slice, reflect.Map, reflect.Array:
		f1_is_zero = fl.Field().IsNil()
	default:
		f1_is_zero = fl.Field().IsZero()
	}

	return f1_is_zero == f2_is_zero
}
